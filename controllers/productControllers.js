const Product = require('../models/Products');
const auth = require("../auth.js");

module.exports.create = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;

	if(isAdmin){
		let newProduct = new Product({
			name : req.body.name,
			description: req.body.description,
			price: req.body.price,
			imageURL : req.body.imageURL
		});

		newProduct.save().then(createdProduct => {
			 res.status(200).send(true);
		}).catch(err => {
			 res.status(500).send({err : "Product not created"});
			console.log(err);
		});
	}else{
		res.status(400).send({err : "User does not have access"});
	}	
}

module.exports.getAllActive = (req, res) =>{
		Product.find({isActive: true}).then(retrievedProducts =>{
			res.status(200).send(retrievedProducts);
		}).catch(error =>{
			res.status(200).send({err : error});
		});

}

module.exports.getAll = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;
	if(isAdmin){
	Product.find({}).then(retrievedProducts =>{
		res.status(200).send(retrievedProducts);
	}).catch(err => res.status(500).send(err));
	}else{
		res.status(400).send({err : "User does not have access"});
	}
}

module.exports.getById = (req, res) => {
	 Product.findById(req.params.id).then(retrievedProduct =>{
	 	(retrievedProduct.isActive) ? res.status(200).send(retrievedProduct) : res.status(400).send("Product is unavailable");
	 }).catch(err => res.status(400).send(err));
}

module.exports.update = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;

	if(isAdmin){
		let updatedProduct = {
			name : req.body.name,
			description: req.body.description,
			price: req.body.price,
			imageURL: req.body.imageURL
		};

		Product.findByIdAndUpdate(req.params.id, updatedProduct, {new: true})
		.then(updatedProduct =>{
			res.status(200).send(true);
		}).catch(err => console.log(err));
	}else{
		res.status(400).send("User has no access");
	}
}


module.exports.archive = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;

	if(isAdmin){
		Product.findByIdAndUpdate(req.params.id, {isActive: false}, {new: true})
		.then(archivedProduct =>{
			res.status(200).send(true);
		}).catch(err => res.status(200).send(err));
	}else{
		res.status(200).send({err : "User has no access"});
	}
}

module.exports.restore = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;

	if(isAdmin){
		Product.findByIdAndUpdate(req.params.id, {isActive: true}, {new: true})
		.then(restoredProduct =>{
			res.status(200).send(true);
		}).catch(err => res.status(200).send(err));
	}else{
		res.status(200).send({err : "User has no access"});
	}
}