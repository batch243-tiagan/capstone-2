const User = require('../models/Users');
const auth = require("../auth.js");
const bcrypt = require('bcrypt');

// module.exports.getAllUsers = (req, res) =>{
// 	User.find({},{password: false})
// 	.then(retrievedUsers => res.send(retrievedUsers));
// }

module.exports.getDetails = (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);

	if(userData.isAdmin){
		User.findById(req.params.id).then(retrievedUsers =>{
			res.status(200).send(retrievedUsers);
		}).catch(err => res.status(500).send(err));
	}else{
		res.status(400).send("User does not have access");
	}
}

module.exports.getUserDetail = (req, res) =>{
	const token = req.headers.authorization;
	const userData = auth.decode(token);
	
	return User.findById(userData.id).then(result => {
		result.password = "Confindential";
		return res.send(result)
	}).catch(err => {
		return res.send(err);
	})
}	

module.exports.registerUser = (req, res) => {
	User.find({email : req.body.email})
	.then(retrievedEmail =>{
		if(retrievedEmail.length == 0){

			let newUser = new User({
				email: req.body.email,
				password: bcrypt.hashSync(req.body.password, 5)
			});

			return newUser.save().then(createdUser => {
				res.status(200).send(true);
			}).catch(err => res.status(200).send({err : `User not registered`}));
		}else{
			res.status(200).send({err : `Email already exists`});
		}
	}).catch(err => res.status(500).send({err : err}));
}

module.exports.userLogin = (req, res) =>{
		User.findOne({email : req.body.email})
		.then(retrievedUser => {
			if(retrievedUser != 0){
				const isPasswordCorrect = bcrypt.compareSync(req.body.password, retrievedUser.password);
				if(isPasswordCorrect){
					return res.status(200).send({accessToken : auth.createAccessToken(retrievedUser)});
				}else{
					return res.status(200).send({err : "Password Incorrect"});
				}
			}else{
				return res.status(200).send({err : "User Does not Exists"});
			}
		}).catch(err => {
			res.status(500).send({err : "User Does not Exists"});
			console.log(err);
	});
}

module.exports.changeUserRole = (req, res) =>{
		const userId = req.params.userId;
		const token = req.headers.authorization;
		const userData = auth.decode(token);

		if(userData.isAdmin){
			return User.findById(userId).then(retrievedUser =>{
				return User.findByIdAndUpdate(userId, {isAdmin: !retrievedUser.isAdmin}, {new: true})
				.then(updatedUser => {
					res.status(200).send(`${retrievedUser.email} role changed`);
				}).catch(err => res.status(500).send(err));
			}).catch(err => res.status(500).send(err));
		}else{
			return res.status(400).send("User does not have access");
		}
}