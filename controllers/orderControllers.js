const Order = require('../models/Orders');
const Product = require('../models/Products');
const auth = require("../auth.js");

module.exports.getAll = (req, res) =>{
	const token = req.headers.authorization;
	const isAdmin = auth.decode(token).isAdmin;
	if(isAdmin){
		Order.find({}).then(retrievedOrders =>{
			res.status(200).send(retrievedOrders);
		}).catch(err => res.status(500).send(err));
	}else{
		res.status(400).send("User does not have access");
	}
}

module.exports.getAllOfUser = (req, res) =>{
	const token = req.headers.authorization;
	const user = auth.decode(token);
	if(!user.isAdmin){
		Order.find({userId: user.id}).then(retrievedOrders =>{
			res.status(200).send(retrievedOrders);
		}).catch(err => res.status(200).send(err));
	}else{
		res.status(200).send({err : "User does not have access"});
	}
}

module.exports.get = (req, res) =>{
	Order.findById(req.params.id).then(retrievedOrder =>{
		if(retrievedOrder != null){
			res.status(200).send(retrievedOrder);
		}else{
			res.status(400).send(`Order ${req.params.id} does not exist`);
		}
	}).catch(err => res.status(500).send(err));
}

module.exports.create = (req, res) =>{
	const token = req.headers.authorization;
	const user = auth.decode(token);
	if(!user.isAdmin){
		new Order({
				userId : user.id
		}).save().then(createdOrder =>{
			res.status(200).send(true);
		}).catch(err => res.status(500).send(err));
	}else{
		res.status(200).send({err : "This is a User only function"});
	}
}

module.exports.addProduct = async (req, res) =>{
	const token = req.headers.authorization;
	const user = auth.decode(token);

	if(!user.isAdmin){
		const order = await Order.findById(req.params.orderId)
		.then(retrievedOrder => retrievedOrder);
		const product = await Product.findById(req.body.productId)
		.then(retrievedProduct => retrievedProduct);
		if(order.purchasedOn == undefined){
			if(order.products.some(e => e.productId === req.body.productId && order.userId == user.id)){
				const indexOfProduct = order.products.findIndex(e => e.productId === req.body.productId && order.userId == user.id);
				order.products[indexOfProduct].quantity += req.body.quantity;
				order.amountToPay += req.body.quantity * product.price;
			}else{
				let insertedProduct = {
					productId: product.id,
					name: product.name,
					description: product.description,
					price: product.price,
					quantity: req.body.quantity,
					imageURL: product.imageURL
				}

				if(order.amountToPay != undefined){
					order.amountToPay += req.body.quantity * product.price;
				}else{
					order.amountToPay = req.body.quantity * product.price;
				}
				order.products.push(insertedProduct);
			}

			Order.findByIdAndUpdate(req.params.orderId, order, {new: true})
			.then(updatedOrder => res.status(200).send(true))
			.catch(err => res.status(200).send(err));
		}else{
			res.status(200).send({err : `You have already finished the transaction for ${order.id} it was purchased on ${order.purchasedOn}`});
		}
	}else{
		res.status(200).send({err : "This is a user only function"});
	}
}


module.exports.makePurchase = async (req, res) =>{
	const token = req.headers.authorization;
	const user = auth.decode(token);

	if(!user.isAdmin){
		const order = await Order.findById(req.params.orderId)
		.then(retrievedOrder => retrievedOrder);
		
		if(order.purchasedOn == undefined){
			order.purchasedOn = new Date();

			order.save().then(updatedOrder => res.status(200).send(true)).
			catch(err => res.status(500).send(err));
		}else{
			res.status(200).send({err: `You have already finished the transaction for ${order.id} it was purchased on ${order.purchasedOn}`});
		}

	}else{
		res.status(400).send("Function is user only");
	}
}