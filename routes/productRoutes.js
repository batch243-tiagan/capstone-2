const express = require('express');
const auth = require("../auth.js");
const router = express.Router();

const productController = require('../controllers/productControllers');

router.post("/create", auth.verify, productController.create);

router.get("/getAllActive", productController.getAllActive);

router.get("/getAll", auth.verify, productController.getAll);

router.get("/get/:id", auth.verify, productController.getById);

router.patch("/update/:id", auth.verify, productController.update);

router.patch("/archive/:id", auth.verify, productController.archive);

router.patch("/restore/:id", auth.verify, productController.restore);

module.exports = router;