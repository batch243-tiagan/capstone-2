const express = require('express');
const auth = require("../auth.js");
const router = express.Router();

const userController = require('../controllers/userControllers');

// router.get("/getAllUsers", userController.getAllUsers);

router.get("/getUserDetail", userController.getUserDetail);

router.post("/register", userController.registerUser);

router.post("/login", userController.userLogin);

router.patch("/changeUserRole/:userId", auth.verify, userController.changeUserRole);

router.get("/getDetails/:id", auth.verify, userController.getDetails);

module.exports = router;