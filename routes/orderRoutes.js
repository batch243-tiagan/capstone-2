const express = require('express');
const router = express.Router();
const auth = require("../auth.js");

const orderController = require('../controllers/orderControllers');

router.get("/getAll", auth.verify, orderController.getAll);

router.get("/getAllOfUser", auth.verify, orderController.getAllOfUser);

router.post("/create", auth.verify, orderController.create);

router.get("/get/:id", auth.verify, orderController.get);

router.patch("/addProduct/:orderId", auth.verify, orderController.addProduct);

router.patch("/makePurchase/:orderId", auth.verify, orderController.makePurchase);

module.exports = router;