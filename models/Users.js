const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	
	email:{
		type: "String",
		require: [true, "emai is required"]
	},
	password:{
		type: "String",
		require: [true, "password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	}

});

module.exports = mongoose.model("Users", userSchema);