const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
	
	userId:{
		type: "String",
		require: [true, "userId is required"]
	},
	products:[{
			productId:{
				type: "String",
				require: [true, "productId is required"]
			},
			name:{
				type: "String",
				require: [true, "name is required"]
			},
			description:{
				type: "String",
				require: [true, "description is required"]
			},
			price:{
				type: Number,
				require: [true, "price is required"]
			},
			quantity:{
				type: Number,
				require: [true, "quantity is required"]
			},
			imageURL:{
				type: "String",
				default: "https://via.placeholder.com/100"
			}
	}],
	amountToPay:{
		type: Number,
		require: [true, "amountToPay is required"]
	},
	purchasedOn:{
		type: Date
	}
});

module.exports = mongoose.model("Orders", orderSchema);